import 'package:bio_bros/services/AnalyticsService.dart';
import 'package:bio_bros/services/AuthService.dart';
import 'package:bio_bros/services/FCMNotificationService.dart';
import 'package:bio_bros/services/FormatterService.dart';
import 'package:bio_bros/services/ModalService.dart';
import 'package:bio_bros/services/StorageService.dart';
import 'package:bio_bros/services/StripeCardService.dart';
import 'package:bio_bros/services/StripeChargeService.dart';
import 'package:bio_bros/services/StripeCouponService.dart';
import 'package:bio_bros/services/StripeCustomerService.dart';
import 'package:bio_bros/services/StripeOrderService.dart';
import 'package:bio_bros/services/StripePlanService.dart';
import 'package:bio_bros/services/StripeSkuService.dart';
import 'package:bio_bros/services/StripeSubscriptionService.dart';
import 'package:bio_bros/services/StripeTokenService.dart';
import 'package:bio_bros/services/UserService.dart';
import 'package:bio_bros/services/ValidatorService.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.I;

void setUpLocater() {
  locator.registerLazySingleton(() => AnalyticsService());
  locator.registerLazySingleton(() => AuthService());
  locator.registerLazySingleton(() => FCMNotificationService());
  locator.registerLazySingleton(() => FormatterService());
  locator.registerLazySingleton(() => ModalService());
  locator.registerLazySingleton(() => StorageService());
  locator.registerLazySingleton(() => StripeCardService());
  locator.registerLazySingleton(() => StripeChargeService());
  locator.registerLazySingleton(() => StripeCouponService());
  locator.registerLazySingleton(() => StripeCustomerService());
  locator.registerLazySingleton(() => StripeOrderService());
  locator.registerLazySingleton(() => StripePlanService());
  locator.registerLazySingleton(() => StripeSkuService());
  locator.registerLazySingleton(() => StripeSubscriptionService());
  locator.registerLazySingleton(() => StripeTokenService());
  locator.registerLazySingleton(() => UserService());
  locator.registerLazySingleton(() => ValidatorService());
}
