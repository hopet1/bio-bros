import 'package:flutter/material.dart';

import 'constants.dart';

class PrivacyPolicyPage extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final TextStyle _headerTextStyle = TextStyle(
    fontSize: 21,
    fontWeight: FontWeight.bold,
  );

  final TextStyle _paragraphTextStyle = TextStyle(
    fontSize: 18,
  );

  final SizedBox _space = SizedBox(
    height: 30,
  );
  final SizedBox _smallSpace = SizedBox(
    height: 10,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        title: Text('Privacy Policy'),
      ),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: ListView(
            children: <Widget>[
              _smallSpace,
              Text(
                'Information Collection and Use',
                style: _headerTextStyle,
              ),
              _smallSpace,
              Text(
                'For a better experience, while using our Service, I may require you to provide us with certain personally identifiable information, including but not limited to $COMPANY_EMAIL. The information that I request will be retained on your device and is not collected by me in any way. The app does use third party services that may collect information used to identify you.',
                style: _paragraphTextStyle,
              ),
              _space,
              Text(
                'Log Data',
                style: _headerTextStyle,
              ),
              _smallSpace,
              Text(
                'I want to inform you that whenever you use my Service, in a case of an error in the app I collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (â€œIPâ€) address, device name, operating system version, the configuration of the app when utilizing my Service, the time and date of your use of the Service, and other statistics.',
                style: _paragraphTextStyle,
              ),
              _space,
              Text(
                'Cookies',
                style: _headerTextStyle,
              ),
              _smallSpace,
              Text(
                'Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers. These are sent to your browser from the websites that you visit and are stored on your device\'s internal memory. This Service does not use these â€œcookiesâ€ explicitly. However, the app may use third party code and libraries that use â€œcookiesâ€ to collect information and improve their services. You have the option to either accept or refuse these cookies and know when a cookie is being sent to your device. If you choose to refuse our cookies, you may not be able to use some portions of this Service.',
                style: _paragraphTextStyle,
              ),
              _space,
              Text(
                'Service Providers',
                style: _headerTextStyle,
              ),
              _smallSpace,
              Text(
                'I may employ third-party companies and individuals due to the following reasons: \n\n* To facilitate our Service;\n* To provide the Service on our behalf;\n* To perform Service-related services; or\n* To assist us in analyzing how our Service is used.\n\nI want to inform users of this Service that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.',
                style: _paragraphTextStyle,
              ),
              _space,
              Text(
                'Security',
                style: _headerTextStyle,
              ),
              _smallSpace,
              Text(
                'I value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and I cannot guarantee its absolute security.',
                style: _paragraphTextStyle,
              ),
              _space,
              Text(
                'Links to Other Sites',
                style: _headerTextStyle,
              ),
              _smallSpace,
              Text(
                'This Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by me. Therefore, I strongly advise you to review the Privacy Policy of these websites. I have no control over and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.',
                style: _paragraphTextStyle,
              ),
              _space,
              Text(
                'Childrenâ€™s Privacy',
                style: _headerTextStyle,
              ),
              _smallSpace,
              Text(
                'These Services do not address anyone under the age of 13. I do not knowingly collect personally identifiable information from children under 13\. In the case I discover that a child under 13 has provided me with personal information, I immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact me so that I will be able to do necessary actions.',
                style: _paragraphTextStyle,
              ),
              _space,
              Text(
                'Changes to This Privacy Policy',
                style: _headerTextStyle,
              ),
              _smallSpace,
              Text(
                'I may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. I will notify you of any changes by posting the new Privacy Policy on this page. This policy is effective as of 2020-09-11.',
                style: _paragraphTextStyle,
              ),
              _space,
              Text(
                'Contact Us',
                style: _headerTextStyle,
              ),
              _smallSpace,
              Text(
                'If you have any questions or suggestions about my Privacy Policy, do not hesitate to contact us at $COMPANY_EMAIL.',
                style: _paragraphTextStyle,
              ),
              _space,
            ],
          ),
        ),
      ),
    );
  }
}
