import 'package:algolia/algolia.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class StoreModel {
  String id;
  String name;
  String imgUrl;

  StoreModel({
    @required this.id,
    @required this.name,
    @required this.imgUrl,
  });

  factory StoreModel.fromDocumentSnapshot({@required DocumentSnapshot ds}) {
    return StoreModel(
        id: ds.data['id'], name: ds.data['name'], imgUrl: ds.data['imgUrl']);
  }

  factory StoreModel.extractAlgoliaObjectSnapshot(
      {@required AlgoliaObjectSnapshot aob}) {
    Map<String, dynamic> data = aob.data;
    return StoreModel(
      id: data['id'],
      name: data['name'],
      imgUrl: data['imgUrl'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'imgUrl': imgUrl,
    };
  }
}
