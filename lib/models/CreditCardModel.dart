import 'package:flutter/material.dart';

class CreditCardModel {
  String id;
  String brand;
  String country;
  int expMonth;
  int expYear;
  String last4;
  String name;

  CreditCardModel({
    @required this.id,
    @required this.brand,
    @required this.country,
    @required this.expMonth,
    @required this.expYear,
    @required this.last4,
    @required this.name,
  });

  factory CreditCardModel.fromMap({@required Map map}) {
    return map == null
        ? map
        : CreditCardModel(
            brand: map['brand'],
            expYear: map['exp_year'],
            expMonth: map['exp_month'],
            last4: map['last4'],
            name: map['name'],
            country: map['country'],
            id: map['id'],
          );
  }
}
