import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class OrderItemModel {
  int amount;
  String id;
  String name;
  int quantity;
  String imgUrl;

  OrderItemModel({
    @required this.amount,
    @required this.id,
    @required this.name,
    @required this.quantity,
    @required this.imgUrl,
  });

  factory OrderItemModel.fromDocumentSnapshot({@required DocumentSnapshot ds}) {
    return OrderItemModel(
      amount: ds.data['amount'] as int,
      id: ds.data['id'] as String,
      name: ds.data['name'] as String,
      quantity: ds.data['quantity'] as int,
      imgUrl: ds.data['imgUrl'] as String,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'amount': amount,
      'id': id,
      'name': name,
      'quantity': quantity,
      'imgUrl': imgUrl,
    };
  }
}
