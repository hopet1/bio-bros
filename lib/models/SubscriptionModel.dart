import 'package:flutter/material.dart';

class SubscriptionModel {
  final DateTime created;
  final String id;
  final String status;

  SubscriptionModel(
      {@required this.created, @required this.id, @required this.status});

  factory SubscriptionModel.fromMap({@required Map map}) {
    return map == null
        ? map
        : SubscriptionModel(
            created: DateTime.fromMillisecondsSinceEpoch(map['created'] * 1000),
            id: map['id'],
            status: map['status']
            // product: map['product'],
            // price: map['price'],
            // updated: DateTime.fromMillisecondsSinceEpoch(map['updated'] * 1000),
            );
  }
}
