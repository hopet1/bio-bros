import 'package:flutter/material.dart';

class ChargeModel {
  String id;
  double amount;
  DateTime created;
  String description;

  ChargeModel(
      {@required this.id,
      @required this.amount,
      @required this.created,
      @required this.description});

  factory ChargeModel.fromMap({@required Map map}) {
    return map == null
        ? map
        : ChargeModel(
            description: map['description'],
            amount: map['amount'] / 100,
            created: DateTime.fromMillisecondsSinceEpoch(map['created'] * 1000),
            id: map['id'],
          );
  }
}
