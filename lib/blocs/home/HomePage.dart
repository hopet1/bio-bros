import 'package:bio_bros/ServiceLocator.dart';
import 'package:bio_bros/services/AuthService.dart';
import 'package:bio_bros/widgets/FullWidthButtonWidget.dart';
import 'package:bio_bros/widgets/SpinnerWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'Bloc.dart';

class HomePage extends StatefulWidget {
  @override
  State createState() => HomePageState();
}

class HomePageState extends State<HomePage> implements HomeBlocDelegate {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  HomeBloc _homeBloc;

  @override
  void initState() {
    _homeBloc = BlocProvider.of<HomeBloc>(context);
    _homeBloc.setDelegate(delegate: this);

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      builder: (BuildContext context, HomeState state) {
        if (state is LoadingState) {
          return Container(
            color: Colors.white,
            child: SpinnerWidget(),
          );
        }

        if (state is LoadedState) {
          return Scaffold(
            key: _scaffoldKey,
            appBar: AppBar(
              title: Text('Home'),
            ),
            // drawer: DrawerWidget(
            //   currentUser: state.user,
            //   page: APP_PAGES.HOME,
            // ),
            body: AnnotatedRegion<SystemUiOverlayStyle>(
              value: SystemUiOverlayStyle.light,
              child: Center(
                child: FullWidthButtonWidget(
                  buttonColor: Colors.black,
                  textColor: Colors.white,
                  text: 'Sign Out',
                  onPressed: () {
                    locator<AuthService>().signOut();
                  },
                ),
              ),
            ),
          );
        }

        // if (state is ErrorState) {
        //   return Container(
        //     child: Center(
        //       child: Text(
        //         state.error.toString(),
        //       ),
        //     ),
        //   );
        // }

        return Container();
      },
    );
  }

  @override
  void showMessage({String message}) {
    // locator<ModalService>()
    //     .showInSnackBar(scaffoldKey: _scaffoldKey, message: message);
  }
}
