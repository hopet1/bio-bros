import 'package:bio_bros/models/UserModel.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class HomeState extends Equatable {
  @override
  List<Object> get props => [];
}

class LoadedState extends HomeState {
  final UserModel user;

  LoadedState({
    @required this.user,
  });

  @override
  List<Object> get props => [
        user,
      ];
}

class LoadingState extends HomeState {
  LoadingState();

  @override
  List<Object> get props => [];
}
