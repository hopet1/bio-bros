import 'package:bio_bros/ServiceLocator.dart';
import 'package:bio_bros/models/UserModel.dart';
import 'package:bio_bros/services/AuthService.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'Bloc.dart';
import 'dart:async';

abstract class HomeBlocDelegate {
  void showMessage({@required String message});
}

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc() : super(null);
  HomeBlocDelegate _homeBlocDelegate;

  UserModel _currentUser;

  void setDelegate({
    @required HomeBlocDelegate delegate,
  }) {
    this._homeBlocDelegate = delegate;
  }

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is LoadPageEvent) {
      yield LoadingState();

      _currentUser = await locator<AuthService>().getCurrentUser();

      yield LoadedState(user: _currentUser);
    }
  }
}
