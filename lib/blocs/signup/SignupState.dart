import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class SignupState extends Equatable {
  @override
  List<Object> get props => [];
}

class LoadedState extends SignupState {
  final bool autoValidate;
  final GlobalKey<FormState> formKey;

  LoadedState({
    @required this.autoValidate,
    @required this.formKey,
  });

  @override
  List<Object> get props => [
        autoValidate,
        formKey,
      ];
}

class SigningIn extends SignupState {}
