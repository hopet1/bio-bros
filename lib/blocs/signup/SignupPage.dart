import 'package:bio_bros/constants.dart';
import 'package:bio_bros/extensions/HexColorExtension.dart';
import 'package:bio_bros/services/ModalService.dart';
import 'package:bio_bros/widgets/FullWidthButtonWidget.dart';
import 'package:bio_bros/widgets/SpinnerWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../ServiceLocator.dart';
import 'Bloc.dart';
import 'package:bio_bros/blocs/login/Bloc.dart' as LOGIN_BP;

class SignupPage extends StatefulWidget {
  @override
  State createState() => SignupPageState();
}

class SignupPageState extends State<SignupPage>
    with SingleTickerProviderStateMixin
    implements SignupBlocDelegate {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  SignupBloc _signupBloc;

  @override
  void initState() {
    _signupBloc = BlocProvider.of<SignupBloc>(context);
    _signupBloc.setDelegate(delegate: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;
    final double screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      key: _scaffoldKey,
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: Container(
          width: screenWidth,
          height: screenHeight,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(ASSET_IMAGE_SPLASH_BACKGROUND),
              fit: BoxFit.cover,
            ),
          ),
          child: Stack(
            children: [
              Container(
                width: screenWidth,
                height: screenHeight,
                color: Colors.black.withOpacity(0.7),
              ),
              SafeArea(
                child: BlocBuilder<SignupBloc, SignupState>(
                  builder: (BuildContext context, SignupState state) {
                    if (state is SigningIn) {
                      return SpinnerWidget();
                    }

                    if (state is LoadedState) {
                      return Form(
                        autovalidate: state.autoValidate,
                        key: state.formKey,
                        child: Column(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Padding(
                                padding: EdgeInsets.only(top: 50),
                                child: Text(
                                  'Bio Bros',
                                  style: TextStyle(
                                    fontSize: 32,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                              child: TextFormField(
                                autovalidate: state.autoValidate,
                                controller: _firstNameController,
                                style: TextStyle(color: Colors.white),
                                decoration: InputDecoration(
                                    prefixIcon: Icon(
                                      Icons.person,
                                      color: Colors.white,
                                    ),
                                    border: OutlineInputBorder(
                                      // width: 0.0 produces a thin "hairline" border
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(90.0),
                                      ),
                                      borderSide: BorderSide.none,

                                      //borderSide: const BorderSide(),
                                    ),
                                    hintStyle: TextStyle(
                                        color: Colors.white,
                                        fontFamily: "WorkSansLight"),
                                    filled: true,
                                    fillColor: Colors.white24,
                                    hintText: 'First Name'),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                              child: TextFormField(
                                autovalidate: state.autoValidate,
                                controller: _lastNameController,
                                style: TextStyle(color: Colors.white),
                                decoration: InputDecoration(
                                    prefixIcon: Icon(
                                      Icons.person,
                                      color: Colors.white,
                                    ),
                                    border: OutlineInputBorder(
                                      // width: 0.0 produces a thin "hairline" border
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(90.0),
                                      ),
                                      borderSide: BorderSide.none,

                                      //borderSide: const BorderSide(),
                                    ),
                                    hintStyle: TextStyle(
                                        color: Colors.white,
                                        fontFamily: "WorkSansLight"),
                                    filled: true,
                                    fillColor: Colors.white24,
                                    hintText: 'Last Name'),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                              child: TextFormField(
                                autovalidate: state.autoValidate,
                                controller: _emailController,
                                style: TextStyle(color: Colors.white),
                                decoration: InputDecoration(
                                    prefixIcon: Icon(
                                      Icons.alternate_email,
                                      color: Colors.white,
                                    ),
                                    border: OutlineInputBorder(
                                      // width: 0.0 produces a thin "hairline" border
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(90.0),
                                      ),
                                      borderSide: BorderSide.none,

                                      //borderSide: const BorderSide(),
                                    ),
                                    hintStyle: TextStyle(
                                        color: Colors.white,
                                        fontFamily: "WorkSansLight"),
                                    filled: true,
                                    fillColor: Colors.white24,
                                    hintText: 'Email'),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                              child: TextFormField(
                                autovalidate: state.autoValidate,
                                controller: _passwordController,
                                style: TextStyle(color: Colors.white),
                                decoration: InputDecoration(
                                    prefixIcon: Icon(
                                      Icons.lock,
                                      color: Colors.white,
                                    ),
                                    border: OutlineInputBorder(
                                      // width: 0.0 produces a thin "hairline" border
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(90.0),
                                      ),
                                      borderSide: BorderSide.none,

                                      //borderSide: const BorderSide(),
                                    ),
                                    hintStyle: TextStyle(
                                        color: Colors.white,
                                        fontFamily: "WorkSansLight"),
                                    filled: true,
                                    fillColor: Colors.white24,
                                    hintText: 'Password'),
                              ),
                            ),
                            Spacer(),
                            FullWidthButtonWidget(
                              buttonColor: Colors.greenAccent,
                              text: 'Sign Up',
                              textColor: Colors.blue.shade900,
                              onPressed: () async {
                                bool confirm = await locator<ModalService>()
                                    .showConfirmation(
                                        context: context,
                                        title: 'Submit',
                                        message: 'Are you sure?');

                                if (!confirm) return;

                                final String email = _emailController.text;
                                final String password =
                                    _passwordController.text;
                                final String firstName =
                                    _firstNameController.text;
                                final String lastName =
                                    _lastNameController.text;

                                _signupBloc.add(
                                  Signup(
                                    email: email,
                                    password: password,
                                    firstName: firstName,
                                    lastName: lastName,
                                    formKey: state.formKey,
                                  ),
                                );
                              },
                            ),
                            Padding(
                              padding: EdgeInsets.all(20),
                              child: InkWell(
                                onTap: () {
                                  Route route = MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        BlocProvider(
                                      create: (BuildContext context) =>
                                          LOGIN_BP.LoginBloc()
                                            ..add(
                                              LOGIN_BP.LoadPageEvent(),
                                            ),
                                      child: LOGIN_BP.LoginPage(),
                                    ),
                                  );
                                  Navigator.pushReplacement(context, route);
                                },
                                child: RichText(
                                  text: TextSpan(
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                    children: [
                                      TextSpan(
                                          text: 'Already have an account?',
                                          style: TextStyle(color: Colors.grey)),
                                      TextSpan(text: ' Log in')
                                    ],
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      );
                    }

                    return Container();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void navigateHome() {
    Navigator.popUntil(
      context,
      ModalRoute.withName(Navigator.defaultRouteName),
    );
  }

  @override
  void showMessage({String message}) {
    locator<ModalService>()
        .showAlert(context: context, title: 'Error', message: message);
  }

  @override
  void clearForm() {
    this._firstNameController.clear();
    this._lastNameController.clear();
    this._emailController.clear();
    this._passwordController.clear();
  }
}
