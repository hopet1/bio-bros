import 'package:bio_bros/models/UserModel.dart';
import 'package:bio_bros/services/AuthService.dart';
import 'package:bio_bros/services/StripeCustomerService.dart';
import 'package:bio_bros/services/UserService.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'dart:async';
import '../../ServiceLocator.dart';
import '../../constants.dart';
import 'SignupEvent.dart';
import 'SignupState.dart';

abstract class SignupBlocDelegate {
  void navigateHome();
  void showMessage({@required String message});
  void clearForm();
}

class SignupBloc extends Bloc<SignupEvent, SignupState> {
  SignupBloc() : super(null);
  SignupBlocDelegate _signupBlocDelegate;

  void setDelegate({
    @required SignupBlocDelegate delegate,
  }) {
    this._signupBlocDelegate = delegate;
  }

  @override
  Stream<SignupState> mapEventToState(SignupEvent event) async* {
    if (event is LoadPageEvent) {
      yield LoadedState(
        autoValidate: false,
        formKey: GlobalKey<FormState>(),
      );
    }

    if (event is Signup) {
      UserModel user;
      final String email = event.email;
      final String password = event.password;
      final String firstName = event.firstName;
      final String lastName = event.lastName;
      final GlobalKey<FormState> formKey = event.formKey;

      if (formKey.currentState.validate()) {
        try {
          yield SigningIn();

          final String customerID =
              await locator<StripeCustomerService>().create(
            email: email,
            description: '$email for new customer.',
            name: '$firstName $lastName',
          );

          AuthResult authResult =
              await locator<AuthService>().createUserWithEmailAndPassword(
            email: email,
            password: password,
          );

          final FirebaseUser firebaseUser = authResult.user;

          user = UserModel(
            imgUrl: DUMMY_PROFILE_PHOTO_URL,
            isAdmin: false,
            email: email,
            fcmToken: null,
            created: DateTime.now(),
            uid: firebaseUser.uid,
            firstName: firstName,
            lastName: lastName,
            customerID: customerID,
            phone: null,
          );

          await locator<UserService>().createUser(user: user);

          _signupBlocDelegate.navigateHome();
        } catch (error) {
          _signupBlocDelegate.showMessage(message: error.toString());

          yield LoadedState(
            autoValidate: true,
            formKey: formKey,
          );
        }
      }
    }
  }
}
