const String ASSET_IMAGE_SPLASH_BACKGROUND = "assets/images/splash_bg.jpg";
const String ASSET_IMAGE_LOGO = 'assets/images/splash_logo.png';
const String ASSET_IMAGE_P2K_LOGO = 'assets/images/icon_p2k.png';
const String ASSET_IMAGE_P2K_TEXT = 'assets/images/preschool_text.png';
const String ASSET_IMAGE_OR = 'assets/images/or_logo.png';

const String DUMMY_PROFILE_PHOTO_URL =
    'https://firebasestorage.googleapis.com/v0/b/hidden-gems-e481d.appspot.com/o/Images%2FUsers%2FDummy%2FProfile.jpg?alt=media&token=99cd4cbd-7df9-4005-adef-b27b3996a6cc';

String version;
String buildNumber;

double screenWidth;
double screenHeight;

const String CLOUD_FUNCTIONS_ENDPOINT =
    'https://us-central1-bio-bros.cloudfunctions.net/';

const List<String> MONTHS = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

const String COMPANY_EMAIL = 'biobros@gmail.com';
