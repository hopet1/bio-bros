import 'package:bio_bros/constants.dart';
import 'package:bio_bros/models/CreditCardModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' show Encoding, json;

abstract class IStripeCardService {
  Future<String> create({@required String customerID, @required String token});
  Future<bool> delete({@required String customerID, @required String cardID});
  Future<List<CreditCardModel>> list({@required String customerID});
}

class StripeCardService extends IStripeCardService {
  @override
  Future<String> create(
      {@required String customerID, @required String token}) async {
    Map data = {'customerID': customerID, 'token': token};

    http.Response response = await http.post(
      '${CLOUD_FUNCTIONS_ENDPOINT}StripeCreateCard',
      body: data,
      headers: {'content-type': 'application/x-www-form-urlencoded'},
    );

    try {
      Map map = json.decode(response.body);
      if (map['statusCode'] == null) {
        return map['id'];
      } else {
        throw PlatformException(
            message: map['raw']['message'], code: map['raw']['code']);
      }
    } catch (e) {
      throw PlatformException(message: e.message, code: e.code);
    }
  }

  @override
  Future<bool> delete(
      {@required String customerID, @required String cardID}) async {
    Map data = {'customerID': customerID, 'cardID': cardID};

    http.Response response = await http.post(
      '${CLOUD_FUNCTIONS_ENDPOINT}StripeDeleteCard',
      body: data,
      headers: {'content-type': 'application/x-www-form-urlencoded'},
    );

    try {
      Map map = json.decode(response.body);
      if (map['statusCode'] == null) {
        return map['deleted'];
      } else {
        throw PlatformException(
            message: map['raw']['message'], code: map['raw']['code']);
      }
    } catch (e) {
      throw PlatformException(message: e.message, code: e.code);
    }
  }

  @override
  Future<List<CreditCardModel>> list({@required String customerID}) async {
    Map data = {'customerID': customerID};

    http.Response response = await http.post(
      '${CLOUD_FUNCTIONS_ENDPOINT}StripeListAllCards',
      body: data,
      headers: {'content-type': 'application/x-www-form-urlencoded'},
    );

    try {
      Map map = json.decode(response.body);
      if (map['statusCode'] == null) {
        List<CreditCardModel> cards = List<CreditCardModel>();
        for (var cardData in map['data']) {
          CreditCardModel card = CreditCardModel.fromMap(map: cardData);
          cards.add(card);
        }
        return cards;
      } else {
        throw PlatformException(
            message: map['raw']['message'], code: map['raw']['code']);
      }
    } catch (e) {
      throw PlatformException(message: e.message, code: e.code);
    }
  }
}
