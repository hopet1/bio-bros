import 'package:bio_bros/constants.dart';
import 'package:bio_bros/models/SubscriptionModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' show json;

abstract class IStripeSubscriptionService {
  Future<SubscriptionModel> retrieve({@required String subscriptionID});
  Future<String> create({@required String customerID, @required String planID});
  Future<bool> cancel({@required String subscriptionID});
}

class StripeSubscriptionService extends IStripeSubscriptionService {
  @override
  Future<SubscriptionModel> retrieve({@required String subscriptionID}) async {
    Map data = {'subscriptionID': subscriptionID};

    http.Response response = await http.post(
      '${CLOUD_FUNCTIONS_ENDPOINT}StripeRetrieveSubscription',
      body: data,
      headers: {'content-type': 'application/x-www-form-urlencoded'},
    );

    try {
      Map map = json.decode(response.body);
      if (map['statusCode'] == null) {
        return SubscriptionModel.fromMap(map: map);
      } else {
        throw PlatformException(
            message: map['raw']['message'], code: map['raw']['code']);
      }
    } catch (e) {
      throw PlatformException(message: e.message, code: e.code);
    }
  }

  @override
  Future<bool> cancel(
      {@required String subscriptionID,
      @required bool invoiceNow,
      @required bool prorate}) async {
    Map data = {
      'subscriptionID': subscriptionID,
      'invoice_now': invoiceNow.toString(),
      'prorate': prorate.toString()
    };

    http.Response response = await http.post(
      '${CLOUD_FUNCTIONS_ENDPOINT}StripeCancelSubscription',
      body: data,
      headers: {'content-type': 'application/x-www-form-urlencoded'},
    );

    try {
      Map map = json.decode(response.body);
      if (map['statusCode'] == null) {
        return map['status'] == 'canceled';
      } else {
        throw PlatformException(
            message: map['raw']['message'], code: map['raw']['code']);
      }
    } catch (e) {
      throw PlatformException(message: e.message, code: e.code);
    }
  }

  @override
  Future<String> create(
      {@required String customerID, @required String planID}) async {
    Map data = {
      'customerID': customerID,
      'plan': planID,
    };

    http.Response response = await http.post(
      '${CLOUD_FUNCTIONS_ENDPOINT}StripeCreateSubscription',
      body: data,
      headers: {'content-type': 'application/x-www-form-urlencoded'},
    );

    try {
      Map map = json.decode(response.body);
      if (map['statusCode'] == null) {
        return map['id'];
      } else {
        throw PlatformException(
            message: map['raw']['message'], code: map['raw']['code']);
      }
    } catch (e) {
      throw PlatformException(message: e.message, code: e.code);
    }
  }
}
