import 'package:bio_bros/constants.dart';
import 'package:bio_bros/models/ChargeModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' show Encoding, json;

abstract class IStripeChargeService {
  Future<ChargeModel> retrieve({@required String chargeID});
  Future<List<ChargeModel>> list({@required String customerID});

  Future<String> create(
      {@required String customerID,
      @required String description,
      @required double amount});
}

class StripeChargeService extends IStripeChargeService {
  @override
  Future<String> create(
      {@required String customerID,
      @required String description,
      @required double amount}) async {
    Map data = {
      'customerID': customerID,
      'description': description,
      'amount': amount * 100
    };

    http.Response response = await http.post(
      '${CLOUD_FUNCTIONS_ENDPOINT}StripeCreateCard',
      body: data,
      headers: {'content-type': 'application/x-www-form-urlencoded'},
    );

    try {
      Map map = json.decode(response.body);
      if (map['statusCode'] == null) {
        return map['id'];
      } else {
        throw PlatformException(
            message: map['raw']['message'], code: map['raw']['code']);
      }
    } catch (e) {
      throw PlatformException(message: e.message, code: e.code);
    }
  }

  @override
  Future<ChargeModel> retrieve({@required String chargeID}) async {
    Map data = {'chargeID': chargeID};

    http.Response response = await http.post(
      '${CLOUD_FUNCTIONS_ENDPOINT}StripeRetrieveCharge',
      body: data,
      headers: {'content-type': 'application/x-www-form-urlencoded'},
    );

    try {
      Map map = json.decode(response.body);
      if (map['statusCode'] == null) {
        return ChargeModel.fromMap(map: map);
      } else {
        throw PlatformException(
            message: map['raw']['message'], code: map['raw']['code']);
      }
    } catch (e) {
      throw PlatformException(message: e.message, code: e.code);
    }
  }

  @override
  Future<List<ChargeModel>> list({@required String customerID}) async {
    Map data = {'customerID': customerID};

    http.Response response = await http.post(
      '${CLOUD_FUNCTIONS_ENDPOINT}StripeListAllCharges',
      body: data,
      headers: {'content-type': 'application/x-www-form-urlencoded'},
    );

    try {
      Map map = json.decode(response.body);
      if (map['statusCode'] == null) {
        List<ChargeModel> charges = List<ChargeModel>();
        for (var i = 0; i < map['data'].length; i++) {
          Map chargeMap = map['data'][i];
          charges.add(
            ChargeModel.fromMap(map: chargeMap),
          );
        }
        return charges;
      } else {
        throw PlatformException(
            message: map['raw']['message'], code: map['raw']['code']);
      }
    } catch (e) {
      throw PlatformException(message: e.message, code: e.code);
    }
  }
}
