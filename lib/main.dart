import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:package_info/package_info.dart';
import 'ServiceLocator.dart';
import 'constants.dart';
import 'services/AuthService.dart';
import 'blocs/login/Bloc.dart' as LOGIN_BP;
import 'blocs/home/Bloc.dart' as HOME_BP;

FirebaseAnalytics analytics;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  analytics = FirebaseAnalytics();

  Crashlytics.instance.enableInDevMode = true;
  FlutterError.onError = Crashlytics.instance.recordFlutterError;

  setUpLocater();

  PackageInfo packageInfo = await PackageInfo.fromPlatform();
  version = packageInfo.version;
  buildNumber = packageInfo.buildNumber;

  runApp(
    MyApp(),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp],
    );

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.orange,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      navigatorObservers: [
        FirebaseAnalyticsObserver(analytics: analytics),
      ],
      home: StreamBuilder(
        stream: locator<AuthService>().onAuthStateChanged(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          screenWidth = MediaQuery.of(context).size.width;
          screenHeight = MediaQuery.of(context).size.height;

          //locator<AuthService>().signOut();

          final FirebaseUser firebaseUser = snapshot.data;

          return firebaseUser == null
              ? BlocProvider(
                  create: (BuildContext context) => LOGIN_BP.LoginBloc()
                    ..add(
                      LOGIN_BP.LoadPageEvent(),
                    ),
                  child: LOGIN_BP.LoginPage(),
                )
              : BlocProvider(
                  create: (BuildContext context) => HOME_BP.HomeBloc()
                    ..add(
                      HOME_BP.LoadPageEvent(),
                    ),
                  child: HOME_BP.HomePage(),
                );
        },
      ),
    );
  }
}
